package com.thoughtworks.androidbootcamppokedex.ui.landing;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.thoughtworks.androidbootcamppokedex.ui.list.PokemonListActivity;
import com.thoughtworks.androidbootcamppokedex.R;

public class LandingScreenActivity extends AppCompatActivity {

    private static final long SPLASH_DELAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_landing_screen);

        startSplash();
    }

    private void startSplash() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    startNextActivity();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }, SPLASH_DELAY_LENGTH);
    }

    private void startNextActivity() {
        Intent intent = new Intent(this, PokemonListActivity.class);
        this.startActivity(intent);
        this.finish();
    }

}

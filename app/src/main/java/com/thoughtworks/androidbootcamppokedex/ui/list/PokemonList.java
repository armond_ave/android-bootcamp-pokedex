package com.thoughtworks.androidbootcamppokedex.ui.list;

import com.thoughtworks.androidbootcamppokedex.model.Pokemon;

import java.util.List;

public class PokemonList {

    List<Pokemon> pokemon;

    public List<Pokemon> getPokemons() {
        return pokemon;
    }
}

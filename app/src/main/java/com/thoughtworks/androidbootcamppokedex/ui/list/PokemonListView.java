package com.thoughtworks.androidbootcamppokedex.ui.list;

import com.thoughtworks.androidbootcamppokedex.base.BaseView;
import com.thoughtworks.androidbootcamppokedex.model.Pokemon;

import java.util.List;

public interface PokemonListView extends BaseView {

    void displayPokemons(List<Pokemon> pokemons);
}

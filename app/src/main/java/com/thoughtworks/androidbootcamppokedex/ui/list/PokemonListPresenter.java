package com.thoughtworks.androidbootcamppokedex.ui.list;

import android.util.Log;

import com.thoughtworks.androidbootcamppokedex.base.BasePresenter;
import com.thoughtworks.androidbootcamppokedex.base.service.PokemonService;

import javax.inject.Inject;

import rx.Scheduler;
import rx.Subscriber;

public class PokemonListPresenter extends BasePresenter<PokemonListView> {

    private final Scheduler scheduler;
    private final PokemonService pokemonService;

    @Inject
    public PokemonListPresenter(PokemonService pokemonService, Scheduler scheduler) {
        this.pokemonService = pokemonService;
        this.scheduler = scheduler;
    }

    public void onCreateView() {
        pokemonService.getPokemonList().observeOn(scheduler)
                .subscribe(new Subscriber<PokemonList>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.d(this.getClass().getName(), e.toString());
            }

            @Override
            public void onNext(PokemonList pokemonList) {
                getView().displayPokemons(pokemonList.getPokemons());
            }
        });
    }
}

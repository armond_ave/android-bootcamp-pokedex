package com.thoughtworks.androidbootcamppokedex.ui.details;

import android.content.Intent;

import com.thoughtworks.androidbootcamppokedex.base.BasePresenter;
import com.thoughtworks.androidbootcamppokedex.model.Pokemon;

import javax.inject.Inject;

import static com.thoughtworks.androidbootcamppokedex.ui.details.PokemonDetailsActivity.EXTRA_POKEMON_DETAILS;

public class PokemonDetailsPresenter extends BasePresenter<PokemonDetailsView> {

    @Inject
    public PokemonDetailsPresenter() {
    }

    public void onCreate(Intent intent) {
        Pokemon pokemon = (Pokemon) intent.getSerializableExtra(EXTRA_POKEMON_DETAILS);

        getView().showPokemonDetails(pokemon.getName(),
                pokemon.getWeight(),
                pokemon.getHeight(),
                pokemon.getCandy(),
                pokemon.getImg());
    }
}

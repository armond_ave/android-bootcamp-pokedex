package com.thoughtworks.androidbootcamppokedex.ui.details;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.thoughtworks.androidbootcamppokedex.R;
import com.thoughtworks.androidbootcamppokedex.base.BaseActivity;
import com.thoughtworks.androidbootcamppokedex.base.injection.ActivityComponent;

public class PokemonDetailsActivity extends BaseActivity<PokemonDetailsPresenter> implements PokemonDetailsView {

    public static final String EXTRA_POKEMON_DETAILS = "ExtraPokemonDetails";

    private TextView nameTextView;
    private TextView attackTextView;
    private TextView defenceTextView;
    private TextView typeTextView;
    private ImageView pokemonImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nameTextView = (TextView) findViewById(R.id.activity_details_name_text);
        attackTextView = (TextView) findViewById(R.id.activity_details_attack_text);
        defenceTextView = (TextView) findViewById(R.id.activity_details_defence_text);
        typeTextView = (TextView) findViewById(R.id.activity_details_type_text);
        pokemonImage = (ImageView) findViewById(R.id.activity_details_image);

        presenter.attachView(this);
        presenter.onCreate(getIntent());
    }

    @Override
    public void showPokemonDetails(String name, String weight, String defense, String candy, String image) {
        getSupportActionBar().setTitle(name);
        nameTextView.setText(name);
        attackTextView.setText(String.valueOf(weight));
        defenceTextView.setText(String.valueOf(defense));
        typeTextView.setText(candy);

        Picasso.with(this).load(image).resize(dpToPx(200), dpToPx(150)).centerInside().into(pokemonImage);
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int dpToPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
}

package com.thoughtworks.androidbootcamppokedex.ui.list;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.thoughtworks.androidbootcamppokedex.R;
import com.thoughtworks.androidbootcamppokedex.base.BaseActivity;
import com.thoughtworks.androidbootcamppokedex.base.injection.ActivityComponent;
import com.thoughtworks.androidbootcamppokedex.model.Pokemon;

import java.util.List;

public class PokemonListActivity extends BaseActivity<PokemonListPresenter> implements PokemonListView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);

        presenter.attachView(this);
        presenter.onCreateView();
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    public void displayPokemons(List<Pokemon> pokemons) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_list_recycler_view);
        recyclerView.setAdapter(new PokemonListAdapter(pokemons));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}

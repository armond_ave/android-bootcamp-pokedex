package com.thoughtworks.androidbootcamppokedex.ui.details;

import com.thoughtworks.androidbootcamppokedex.base.BaseView;

public interface PokemonDetailsView extends BaseView {

    void showPokemonDetails(String name, String weight, String height, String candy, String image);
}

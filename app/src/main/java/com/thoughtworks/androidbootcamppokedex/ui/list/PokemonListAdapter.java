package com.thoughtworks.androidbootcamppokedex.ui.list;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thoughtworks.androidbootcamppokedex.R;
import com.thoughtworks.androidbootcamppokedex.model.Pokemon;
import com.thoughtworks.androidbootcamppokedex.ui.details.PokemonDetailsActivity;

import java.util.List;

import static com.thoughtworks.androidbootcamppokedex.ui.details.PokemonDetailsActivity.EXTRA_POKEMON_DETAILS;

public class PokemonListAdapter extends RecyclerView.Adapter<PokemonListAdapter.PokemonItemViewHolder> {

    private final List<Pokemon> pokemonList;

    public PokemonListAdapter(List<Pokemon> pokemonList) {
        this.pokemonList = pokemonList;
    }

    @Override
    public PokemonItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pokemon_item_list, parent, false);
        return new PokemonItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PokemonItemViewHolder holder, int position) {
        holder.setItem(pokemonList.get(position));
    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }

    protected static class PokemonItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView typeTextView;
        private final ImageView pokemonImage;

        public PokemonItemViewHolder(View v) {
            super(v);
            nameTextView = (TextView) itemView.findViewById(R.id.pokemon_item_name);
            typeTextView = (TextView) itemView.findViewById(R.id.pokemon_item_type);
            pokemonImage = (ImageView) itemView.findViewById(R.id.pokemon_item_icon);

        }

        void setItem(final Pokemon pokemon) {
            nameTextView.setText(pokemon.getName());
            typeTextView.setText(pokemon.getHeight());

            Picasso.with(itemView.getContext()).load(pokemon.getImg()).into(pokemonImage);


            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Context context = itemView.getContext();
                    context.startActivity(
                            new Intent(context, PokemonDetailsActivity.class).putExtra(EXTRA_POKEMON_DETAILS, pokemon)
                    );
                }
            });
        }
    }
}
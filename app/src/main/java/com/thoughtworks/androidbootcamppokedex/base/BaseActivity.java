package com.thoughtworks.androidbootcamppokedex.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.thoughtworks.androidbootcamppokedex.base.injection.ActivityComponent;
import com.thoughtworks.androidbootcamppokedex.base.injection.ActivityModule;

import javax.inject.Inject;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity {
    @Inject
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActivityComponent activityComponent = ((MyPokedexApplication) getApplication()).getApplicationComponent()
                .newActivityComponent(new ActivityModule());
        inject(activityComponent);
    }

    protected abstract void inject(ActivityComponent activityComponent);
}

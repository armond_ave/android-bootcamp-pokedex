package com.thoughtworks.androidbootcamppokedex.base;

import android.app.Application;

import com.thoughtworks.androidbootcamppokedex.base.injection.ApplicationComponent;
import com.thoughtworks.androidbootcamppokedex.base.injection.ApplicationModule;
import com.thoughtworks.androidbootcamppokedex.base.injection.DaggerApplicationComponent;

public class MyPokedexApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}

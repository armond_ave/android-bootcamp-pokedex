package com.thoughtworks.androidbootcamppokedex.base;

import java.lang.ref.WeakReference;

public class BasePresenter<V extends BaseView> {
    private WeakReference<V> view;

    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    public V getView() {
        return view.get();
    }
}

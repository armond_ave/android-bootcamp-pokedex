package com.thoughtworks.androidbootcamppokedex.base.service;

import com.thoughtworks.androidbootcamppokedex.ui.list.PokemonList;

import retrofit2.http.GET;
import rx.Observable;

public interface PokemonService {

    @GET("pokedex.json")
    Observable<PokemonList> getPokemonList();
}



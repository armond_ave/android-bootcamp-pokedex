package com.thoughtworks.androidbootcamppokedex.base.injection;

import com.thoughtworks.androidbootcamppokedex.base.service.PokemonService;

import dagger.Component;

@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    ActivityComponent newActivityComponent(ActivityModule activityModule);

    PokemonService newPokemonService();
}



package com.thoughtworks.androidbootcamppokedex.base.injection;

import com.thoughtworks.androidbootcamppokedex.ui.details.PokemonDetailsActivity;
import com.thoughtworks.androidbootcamppokedex.ui.list.PokemonListActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(PokemonDetailsActivity detailsActivity);
    void inject(PokemonListActivity pokemonListActivity);

}

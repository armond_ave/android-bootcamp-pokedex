package com.thoughtworks.androidbootcamppokedex.ui.list;

import com.thoughtworks.androidbootcamppokedex.base.service.PokemonService;
import com.thoughtworks.androidbootcamppokedex.model.Pokemon;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PokemonListPresenterTest {

    @Mock
    PokemonService mPokemonService;

    private PokemonListPresenter pokemonListPresenter;

    @Mock
    private PokemonListView view;

    @Mock
    private PokemonList pokemonList;

    @Before
    public void setUp() throws Exception {
        pokemonListPresenter = new PokemonListPresenter(mPokemonService, Schedulers.immediate());
        pokemonListPresenter.attachView(view);
        when(mPokemonService.getPokemonList()).thenReturn(Observable.just(pokemonList));
    }

    @Test
    public void shouldRetrievePokemonList() throws Exception {
        pokemonListPresenter.onCreateView();

        verify(mPokemonService).getPokemonList();
    }

    @Test
    public void shouldDisplayRetrievedPokemons() throws Exception {
        pokemonListPresenter.onCreateView();

        verify(view).displayPokemons(pokemonList.getPokemons());
    }
}
